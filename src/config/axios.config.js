import axios from 'axios';

export default axios.create({
  baseURL: 'http://localhost:3000/blog-api'
});

export const authConfig = {
  headers: { Authorization: `Bearer ${window.localStorage.getItem('token')}` }
};
