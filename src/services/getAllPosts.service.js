import axios from '../config/axios.config';

export const getAllPostsService = async () => {
  try {
    const {
      data: { allPosts }
    } = await axios.get('posts');
    console.log('blog.service.js - getAllPosts allPosts', allPosts);
    if (!allPosts) return null;
    return allPosts;
  } catch (err) {
    console.warn(err);
  }
};
