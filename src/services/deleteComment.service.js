import axios, { authConfig } from '../config/axios.config';

export const deleteCommentService = async (postId, commentId) => {
  try {
    const { data } = await axios.delete(`posts/${postId}/comments/${commentId}`, authConfig);
    console.log('blog.service.js - deleteCommentService data', data);
  } catch (err) {
    console.warn(err);
  }
};
