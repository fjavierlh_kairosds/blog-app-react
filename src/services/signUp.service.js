import axios from '../config/axios.config';

export const signUpUserService = async ({ name, nickname, email, password }) => {
  try {
    const { data } = await axios.post('users/sign-up', {
      name,
      nickname,
      email,
      password
    });
    console.log('blog.service.js - signUpUser data', data);
  } catch (err) {
    console.warn(err);
  }
};
