import axios, { authConfig } from '../config/axios.config';

export const updatePostService = async (id, post) => {
  try {
    const {
      data: { updatedPost }
    } = await axios.put(`posts/${id}`, post, authConfig);
    return updatedPost;
  } catch (err) {
    console.warn(err);
  }
};
