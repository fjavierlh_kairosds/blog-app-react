import axios from '../config/axios.config';

export const getPostByIdService = async (id) => {
  try {
    const { data: { searchedPost } } = await axios.get(`posts/${id}`);
    return searchedPost;
  } catch (err) {
    console.warn(err);
  }
};
