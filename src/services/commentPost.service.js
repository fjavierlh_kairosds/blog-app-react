import axios, { authConfig } from '../config/axios.config';

export const addCommentService = async (id, comment) => {
  try {
    const { data: { newComment } } = await axios.post(`posts/${id}/comments`, comment, authConfig);
    console.log('blog.service.js - addCommentService newComment', newComment);
    return newComment;
  } catch (err) {
    console.warn(err);
  }
};
