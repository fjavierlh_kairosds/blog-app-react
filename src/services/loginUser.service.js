import axios from '../config/axios.config';

export const loginUserService = async ({ email, password }) => {
  try {
    const {
      data: { token }
    } = await axios.post('users/sign-in', { email, password });
    console.log('blog.service.js - signUpUser token', token);
    return token;
  } catch (err) {
    console.warn(err);
  }
};
