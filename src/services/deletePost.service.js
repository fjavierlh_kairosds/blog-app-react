import axios, { authConfig } from '../config/axios.config';

export const deletePostService = async (id) => {
  try {
    const { data } = await axios.delete(`posts/${id}`, authConfig);
    console.log('blog.service.js - deletePost data', data);
  } catch (err) {
    console.warn(err);
  }
};
