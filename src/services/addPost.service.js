import axios, { authConfig } from '../config/axios.config';

export const addPostService = async (post) => {
  try {
    const {
      data: { newPost }
    } = await axios.post('posts', post, authConfig);
    console.log('blog.service.js - addPost newPost', newPost);
    return newPost;
  } catch (err) {
    console.warn(err);
  }
};
