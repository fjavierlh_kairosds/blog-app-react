import PropTypes from 'prop-types';
import React from 'react';

const Footer = ({ children }) => {
  return <footer>{children}</footer>;
};

Footer.propTypes = {
  children: PropTypes.node
};

export default Footer;
