import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const Header = ({ children }) => {
  return (
    <header>
      <BlogTitle>Oh my blog!</BlogTitle>
      {children}
    </header>
  );
};

Header.propTypes = {
  children: PropTypes.node
};

const BlogTitle = styled.h1`
  font-size: 0.8rem;
  color: red;
`;

export default Header;
