import styled from 'styled-components';

const MainLayout = styled.div`
  min-height: 100%;
  background-color: antiquewhite;
`;

export default MainLayout;
