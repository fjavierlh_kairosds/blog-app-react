import PropTypes from 'prop-types';
import React from 'react';

const MainNavBar = ({ children }) => {
  return (
    <nav>
      <ul>{children}</ul>
    </nav>
  );
};

MainNavBar.propTypes = {
  children: PropTypes.node.isRequired
};
export default MainNavBar;
