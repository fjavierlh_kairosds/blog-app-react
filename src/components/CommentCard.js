import PropTypes from 'prop-types';
import React, { useContext } from 'react';
import { AppContext } from '../BlogApp';
const CommentCard = ({ onRemove, postId, comment }) => {
  const { isLoggin, user } = useContext(AppContext);
  return (
    <>
        <h4>{comment.nickname}</h4>
        <p>{comment.content}</p>
        {isLoggin && user.nickname === comment.nickname &&
        (
          <input
            type="submit"
            value="Remove"
            onClick={async () => {
              await onRemove(postId, comment.id);
            }}
          />
        )}
    </>
  );
};

CommentCard.propTypes = {
  onRemove: PropTypes.func,
  postId: PropTypes.string,
  comment: PropTypes.object
};

export default CommentCard;
