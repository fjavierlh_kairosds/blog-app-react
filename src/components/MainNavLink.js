import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';

const MainNavLink = ({ path, text }) => {
  return (
    <li>
      <Link to={path}>{text}</Link>
    </li>
  );
};

MainNavLink.propTypes = {
  path: PropTypes.string,
  text: PropTypes.string
};

export default MainNavLink;
