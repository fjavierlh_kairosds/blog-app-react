import PropTypes from 'prop-types';
import React from 'react';
import { useHistory } from 'react-router';

const SignOutButton = ({ onSignOut, redirectTo }) => {
  const history = useHistory();
  return (
    <input
      type="button"
      onClick={() => { onSignOut(); history.push(`${redirectTo || '/'}`) }}
      value="Sign out"
    />
  );
};

SignOutButton.propTypes = {
  onSignOut: PropTypes.func,
  redirectTo: PropTypes.string
};

export default SignOutButton;
