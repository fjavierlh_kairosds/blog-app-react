import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { AppContext } from '../BlogApp';
import { addPostService } from '../services/addPost.service';
import { updatePostService } from '../services/updatePost.service';

function validateTitle (title) {
  const minLength = 5;
  return {
    result: title.length >= minLength,
    message: `Title length must be at least ${minLength} characters.`
  };
}

function validateContent (content) {
  const minLength = 50;
  const maxLength = 300;
  return {
    result: content.length > minLength && content.length < maxLength,
    message: `Content length must be between ${minLength} and ${maxLength} characters.`
  };
}

const PostForm = () => {
  const { posts, handleAddPost, handleUpdatePost } = useContext(AppContext);
  const [post, setPost] = useState({});
  const searchParams = new URLSearchParams(window.location.search);
  const id = searchParams.get('id');

  useEffect(() => {
    const foundPost = posts.find((post) => post.id === id);
    console.log(`post: ${JSON.stringify(foundPost)}`);
    setPost(oldPost => ({ ...oldPost, ...foundPost }));
  }, []);

  const [titleError, setTitleError] = useState('');
  const [contentError, setContentError] = useState('');
  const history = useHistory();

  const handleChange = (e) => {
    const { name: inputName, value } = e.target;
    setPost({ ...post, [inputName]: value });
  };

  const handleSubmitPublish = async (e) => {
    e.preventDefault();
    const { title, content } = post;
    if (!title || !content) return;
    const newPost = await addPostService(post);
    if (!newPost) return;
    handleAddPost(newPost);
    history.push('/dashboard');
  };

  const handleSubmitUpdate = async (e) => {
    e.preventDefault();
    const { title, content } = post;
    if (!title || !content) return;
    const updatedPost = await updatePostService(id, post);
    if (!updatedPost) return;
    handleUpdatePost(id, updatedPost);
    history.push('/');
  };

  const { title: titleValue, content: contentValue } = post;

  return (
    <form onSubmit={!id ? handleSubmitPublish : handleSubmitUpdate}>
      <label htmlFor="title">Post title</label>
      <input
        type="text"
        value={titleValue}
        onChange={handleChange}
        onBlur={(e) => {
          const { result, message } = validateTitle(e.target.value);
          if (!result) {
            setTitleError(message);
            return;
          }
          setTitleError('');
        }}
        id="title"
        name="title"
      />
      <br />
      {titleError && <small style={{ color: 'red' }}>{titleError}</small>}
      <br />
      <label htmlFor="content">Content</label>
      <textarea
        type="textarea"
        value={contentValue}
        onChange={handleChange}
        onBlur={(e) => {
          const { result, message } = validateContent(e.target.value);
          if (!result) {
            setContentError(message);
            return;
          }
          setContentError('');
        }}
        id="content"
        name="content"
      />
      <br />
      {contentError && <small style={{ color: 'red' }}>{contentError}</small>}
      <br />
      <input type="submit" value={id ? 'Edit' : 'Publish'} />
    </form>
  );
};

export default PostForm;
