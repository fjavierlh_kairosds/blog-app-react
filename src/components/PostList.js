import PropTypes from 'prop-types';
import React, { useContext, useEffect } from 'react';
import { useState } from 'react/cjs/react.development';
import { AppContext } from '../BlogApp';
import PostCard from './PostCard';

function PostsList ({ isAdmin, onDashboard }) {
  const { posts, user, handleRemovePost } = useContext(AppContext);
  const [postList, setPostList] = useState([...posts]);

  useEffect(() => {
    setPostList(postList => (
      isAdmin ?? true ? posts : posts.filter((p) => p.nickname === user.nickname))
    );
  }, [posts]);

  return (
    <ul>
      {postList
        ? postList.map((post) => (
            <PostCard
              key={post.id}
              post={post}
              onDashboard={onDashboard}
              onRemove={handleRemovePost}
            />
        ))
        : 'Posts not found...'}
    </ul>
  );
}

PostsList.propTypes = {
  isAdmin: PropTypes.bool,
  onDashboard: PropTypes.bool,
  posts: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      author: PropTypes.string,
      nickname: PropTypes.string,
      title: PropTypes.string,
      content: PropTypes.string
    })
  )
};

export default PostsList;
