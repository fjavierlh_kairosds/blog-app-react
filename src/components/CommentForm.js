import PropTypes from 'prop-types';
import React, { useContext, useState } from 'react';
import { AppContext } from '../BlogApp';
import { addCommentService } from '../services/commentPost.service';

function validateComment (comment) {
  const minLength = 10;
  return {
    result: comment.length > 10,
    message: `Comment length must be at least ${minLength} character`
  };
}

const CommentForm = ({ id, onComment, post }) => {
  const [comment, setComment] = useState('');
  const [commentError, setCommentError] = useState('');
  const [invalidContentError, setInvalidContentError] = useState('');

  const { isLoggin, user } = useContext(AppContext);
  function handleChange (e) {
    const comment = e.target.value;
    setComment(comment);
  }

  async function handleSubmit (e) {
    e.preventDefault();
    const newComment = { nickname: user.nickname, content: comment };
    const returnedComment = await addCommentService(id, newComment);
    if (!returnedComment) {
      setInvalidContentError('Comment has some offensive word');
      return;
    };
    setInvalidContentError('');
    const newComments = [...post.comments, returnedComment];
    const newPost = { ...post };
    newPost.comments = newComments;
    onComment(newPost);
  }

  return (
    <>
      {isLoggin
        ? (
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            name="comment"
            id="comment"
            value={comment}
            onChange={handleChange}
            onBlur={(e) => {
              const { result, message } = validateComment(e.target.value);
              if (!result) {
                setCommentError(message);
                return;
              }
              setCommentError('');
            }}
          />
          <br />
          {commentError && (
            <small style={{ color: 'red' }}>{commentError}</small>
          )}
          {invalidContentError && (
            <small style={{ color: 'red' }}>{invalidContentError}</small>
          )}
          <br />
          <input type="submit" value="Comment" />
        </form>
          )
        : (
        <h3>To comment you must be login</h3>
          )}
    </>
  );
};

CommentForm.propTypes = {
  id: PropTypes.string,
  onComment: PropTypes.func,
  post: PropTypes.object
};

export default CommentForm;
