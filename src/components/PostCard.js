import PropTypes from 'prop-types';
import React from 'react';
import { Link, useHistory } from 'react-router-dom';

function PostCard ({ post, onDashboard, onRemove, onViewComments }) {
  const { id, title, author, content, comments } = post;
  const countComments = comments ? comments.length : 0;
  const history = useHistory();
  return (
    <li>
      <article>
        <Link to={`/post?id=${id}`}>
          <h1>{title}</h1>
        </Link>
        <small>by {author}</small>
        <p>{content.length < 100 ? content : content.substring(0, 100)}...</p>
        {/* {!onDashboard && <Link to={`/post?id=${id}`}>Read more...</Link>} */}
      </article>
      {onDashboard && (
        <>
          <button onClick={() => onRemove(id)}>Remove</button>
          <button
            onClick={() => {
              history.push(`/edit?id=${id}`);
            }}
          >
            Update
          </button>
          <button onClick={() => history.push}>{countComments} Comments</button>
        </>
      )}
    </li>
  );
}

PostCard.propTypes = {
  post: PropTypes.shape({
    id: PropTypes.string,
    author: PropTypes.string,
    nickname: PropTypes.string,
    title: PropTypes.string,
    content: PropTypes.string,
    comments: PropTypes.arrayOf(PropTypes.shape({
      nickname: PropTypes.string,
      content: PropTypes.string
    }))
  }).isRequired,
  onDashboard: PropTypes.bool,
  onRemove: PropTypes.func,
  onViewComments: PropTypes.func
};

export default PostCard;
