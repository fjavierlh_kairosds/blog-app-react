import React, { useState } from 'react';
import { signUpUserService } from '../services/signUp.service';

const SingUpForm = (props) => {
  const [signUpData, setSignUpData] = useState({
    name: '',
    nickname: '',
    email: '',
    password: ''
  });

  const handleChange = (e) => {
    const { name: inputName, value } = e.target;
    setSignUpData({ ...signUpData, [inputName]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    await signUpUserService(signUpData);
  };

  const { name, nickname, email, password } = signUpData;

  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor="name">Your name</label>
      <input
        type="text"
        value={name}
        onChange={handleChange}
        id="name"
        name="name"
      />
      <label htmlFor="nickname">Nickname</label>
      <input
        type="text"
        value={nickname}
        onChange={handleChange}
        id="nickname"
        name="nickname"
      />
      <label htmlFor="email">Email</label>
      <input
        type="email"
        value={email}
        onChange={handleChange}
        id="email"
        name="email"
      />
      <label htmlFor="Password">Password</label>
      <input
        type="password"
        value={password}
        onChange={handleChange}
        id="password"
        name="password"
      />
      <input type="submit" value="Sign up" />
    </form>
  );
};

export default SingUpForm;
