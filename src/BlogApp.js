import React, { useEffect, useState } from 'react';
import { Link, Route, Switch } from 'react-router-dom';
import MainNavBar from './components/MainNavBar';
import MainNavLink from './components/MainNavLink';
import PostForm from './components/PostForm';
import SignOutButton from './components/SignOutButton';
import MainLayout from './layout/MainLayout';
import Footer from './layout/parts/Footer';
import Header from './layout/parts/Header';
import Main from './layout/parts/MainSection';
import ContextScreen from './screens/ContextScreen';
import DashboardScreen from './screens/DashboardScreen';
import HomeScreen from './screens/HomeScreen';
import LoginScreen from './screens/LoginScreen';
import PageNotFoundScreen from './screens/PageNotFoundScreen';
import PostDetailScreen from './screens/PostDetailScreen';
import SignUpScreen from './screens/SignUpScreen';
import { deletePostService } from './services/deletePost.service';
import { getAllPostsService } from './services/getAllPosts.service';

export const AppContext = React.createContext();

function BlogApp () {
  const [isLoggin, setIsLoggin] = useState(false);
  const [user, setUser] = useState({});
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    getAllPostsService()
      .then((posts) => posts.reverse())
      .then(handleSyncPosts);
  }, []);

  function toggleLogin () {
    setIsLoggin(login => !login);
  }

  function toggleSignOut () {
    toggleLogin();
    setUser({});
  }

  function handleUserData (user) {
    setUser(oldUser => ({ ...oldUser, ...user }));
  }

  function handleSyncPosts (posts) {
    setPosts(oldPosts => [...oldPosts, ...posts]);
  }

  async function handleRemovePost (id) {
    await deletePostService(id);
    setPosts((oldPosts) => oldPosts.filter((post) => post.id !== id));
  }
  function handleAddPost (post) {
    setPosts((oldPosts) => [post, ...oldPosts]);
  }

  function handleUpdatePost (id, post) {
    const i = posts.findIndex((post) => post.id === id);
    setPosts(oldPosts => {
      const newPosts = [...oldPosts];
      newPosts[i] = post;
      return newPosts;
    });
  }

  return (
    <AppContext.Provider
      value={{
        isLoggin,
        user,
        posts,
        toggleLogin,
        handleUserData,
        handleSyncPosts,
        handleRemovePost,
        handleAddPost,
        handleUpdatePost
      }}
    >
      <MainLayout>
        <Header>
          <MainNavBar>
            <MainNavLink path="/" text="Home" />
            {!isLoggin
              ? (
              <MainNavLink path="/login" text="Login" />
                )
              : (
              <>
                <MainNavLink path="/publish" text="Publish post" />
                <MainNavLink path="/dashboard" text="Dashboard" />
                {user.role === 'ADMIN' && (
                  <MainNavLink path="/sign-up" text="Sign up user"/>
                )}
              </>
                )}
          </MainNavBar>
        </Header>

        <Main>
          <Switch>
            <Route path="/sign-up">
              <SignUpScreen />
            </Route>
            <Route path="/post">
              <PostDetailScreen />
            </Route>
            <Route path="/publish">
              <PostForm />
            </Route>
            <Route path="/edit">
              <PostForm />
            </Route>
            <Route path="/context">
              <ContextScreen />
            </Route>
            <Route path="/dashboard">
              <DashboardScreen />
            </Route>
            <Route path="/login">
              <LoginScreen />
            </Route>
            <Route exact path="/">
              <HomeScreen />
            </Route>
            <Route path="*">
              <PageNotFoundScreen />
            </Route>
          </Switch>
        </Main>

        <Footer>
          {isLoggin && <SignOutButton onSignOut={toggleSignOut} />}
          <Link to="/context">View current context</Link>
        </Footer>
      </MainLayout>
    </AppContext.Provider>
  );
}

export default BlogApp;
