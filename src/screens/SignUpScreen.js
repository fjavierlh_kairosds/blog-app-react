import React from 'react';
import SingUpForm from '../components/SingUpForm';

const SignUpScreen = () => {
  return (
    <>
      <h1>Sign up</h1>
      <SingUpForm />
    </>
  );
};

export default SignUpScreen;
