import React from 'react';
import PostsList from '../components/PostList';

const HomeScreen = () => {
  return (
    <>
      <h1>Home</h1>
      <PostsList />
    </>
  );
};

export default HomeScreen;
