import React, { useContext } from 'react';
import { AppContext } from '../BlogApp';
import PostsList from '../components/PostList';

function DashboardScreen () {
  const { isLoggin, user } = useContext(AppContext);
  const isAdmin = user.role === 'ADMIN';

  return (
    <>
      {isLoggin
        ? (
        <>
          <h1>{isAdmin ? 'Admin' : 'Publisher'} dashboard </h1>
          <PostsList
          isAdmin={isAdmin}
          onDashboard={true} />
        </>
          )
        : (
        <h1>Not logged</h1>
          )}
    </>
  );
}

export default DashboardScreen;
