import React, { useContext } from 'react';
import { AppContext } from '../BlogApp';

const ContextScreen = () => {
  const context = useContext(AppContext);
  return <pre>{JSON.stringify(context, null, 2)}</pre>;
};

export default ContextScreen;
