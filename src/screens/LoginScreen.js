// eslint-disable-next-line camelcase
import jwt_decode from 'jwt-decode';
import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { AppContext } from '../BlogApp';
import { loginUserService } from '../services/loginUser.service';

function LoginScreen () {
  const { toggleLogin, handleUserData } = useContext(AppContext);
  const [loginData, setLoginData] = useState({
    email: '',
    password: ''
  });
  const [errorMessage, setErrorMessage] = useState('');
  const history = useHistory();

  const handleChange = (e) => {
    const { name: inputName, value } = e.target;
    setLoginData({ ...loginData, [inputName]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const { email, password } = loginData;
    if (!email || !password) return;

    const receivedToken = await loginUserService({ ...loginData });
    if (!receivedToken) {
      setErrorMessage('Email or password are invalid');
      return;
    };

    toggleLogin();
    window.localStorage.setItem('token', receivedToken);
    const decodedToken = jwt_decode(receivedToken);
    handleUserData(decodedToken);

    history.push('/dashboard');
  };

  return (
    <form onSubmit={(e) => handleSubmit(e)}>
      <h2>{errorMessage}</h2>
      <label htmlFor="email">Email</label>
      <input
        type="email"
        value={loginData.email}
        onChange={(e) => handleChange(e)}
        id="email"
        name="email"
      />
      <label htmlFor="password">Password</label>
      <input
        type="password"
        value={loginData.password}
        onChange={(e) => handleChange(e)}
        id="password"
        name="password"
      />
      <input type="submit" value="Log In" />
    </form>
  );
}
export default LoginScreen;
