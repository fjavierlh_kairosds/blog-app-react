import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import CommentCard from '../components/CommentCard';
import CommentForm from '../components/CommentForm';
import { deleteCommentService } from '../services/deleteComment.service';
import { getPostByIdService } from '../services/getPostById.service';

const PostDetailScreen = () => {
  const [post, setPost] = useState({});

  const searchParams = new URLSearchParams(window.location.search);
  const id = searchParams.get('id');

  useEffect(async () => {
    const searchedPost = await getPostByIdService(id);
    console.log(`searchedPost: ${JSON.stringify(searchedPost)}`);
    setPost((oldPost) => ({ ...oldPost, ...searchedPost }));
  }, []);

  const { title, author, nickname, content, comments } = post;

  async function handleRemoveComment (postId, commentId) {
    await deleteCommentService(postId, commentId);
    const newComments = post.comments.filter(c => c.id !== commentId);
    const newPost = { ...post };
    newPost.comments = newComments;
    setPost(newPost);
  }

  return (
        <>
            <h1>{title}</h1>
            <h3>{author} - {nickname}</h3>
            <p>{content}</p>
            <CommentForm id={id} onComment={setPost} post={post} />
            <h2>Comments</h2>
            {Array.isArray(comments) && comments.length > 0
              ? comments.map(c => {
                return (
                <CommentCard key={c.id} postId={id} onRemove={handleRemoveComment} comment={c} />
                );
              })
              : <h5>Not comments yet :(</h5>}
        </>
  );
};

PostDetailScreen.propTypes = {
  post: PropTypes.object
};

export default PostDetailScreen;
